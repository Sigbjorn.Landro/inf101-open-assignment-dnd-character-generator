package DnDCharacterGenerator.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;

import DnDCharacterGenerator.DnDCharacterSheets.CharacterSheetModel;

public class GUImanager implements ActionListener {

    JFrame frame;

    CharacterSheetModel model;
    DnDCharacterView view;


    JButton levelUpButton;

    // buttons for increasing stats when you have available ASI
    JButton strUpButton;
    JButton dexUpButton;
    JButton conUpButton;
    JButton intUpButton;
    JButton wisUpButton;
    JButton chaUpButton;
    
    // buttons for assigning skills when you have unassigned skills
    JButton assAcroButton;
    JButton assAHButton;
    JButton assArcaButton;
    JButton assAthlButton;
    JButton assDecButton;
    JButton assHistButton;
    JButton assInsButton;
    JButton assIntiButton;
    JButton assMedButton;
    JButton assNatButton;
    JButton assPercepButton;
    JButton assPerforButton;
    JButton assPersuaButton;
    JButton assRelButton;
    JButton assSoHButton;
    JButton assSteaButton;
    JButton assSurvButton;
    
    public static final String WINDOW_TITLE = "INF101 Open Assignment - DnD Character Generator";

    public GUImanager(CharacterSheetModel model, JFrame frame) {
        this.model = model;
        this.view = new DnDCharacterView(model);
        this.frame = frame;
    }

    public void initialize() {
        strUpButton = new JButton("Str +");
        strUpButton.setBounds(160,95, 70, 20);
        frame.add(strUpButton);
        strUpButton.addActionListener(this);

        JButton dexUpButton = new JButton("Dex +");
        dexUpButton.setBounds(160,115, 70, 20);
        frame.add(dexUpButton);
        dexUpButton.addActionListener(this);

        JButton conUpButton = new JButton("Con +");
        conUpButton.setBounds(160,135, 70, 20);
        frame.add(conUpButton);
        conUpButton.addActionListener(this);

        JButton intUpButton = new JButton("Int +");
        intUpButton.setBounds(160,155, 70, 20);
        frame.add(intUpButton);
        intUpButton.addActionListener(this);

        JButton wisUpButton = new JButton("Wis +");
        wisUpButton.setBounds(160,175, 70, 20);
        frame.add(wisUpButton);
        wisUpButton.addActionListener(this);

        JButton chaUpButton = new JButton("Cha +");
        chaUpButton.setBounds(160,195, 70, 20);
        frame.add(chaUpButton);
        chaUpButton.addActionListener(this);



    }


    public static void run(CharacterSheetModel model) {
        JFrame frame = new JFrame(WINDOW_TITLE);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        GUImanager guiM = new GUImanager(model, frame);

        guiM.initialize();
        
        frame.setContentPane(guiM.view);

        frame.pack();
        frame.setVisible(true);
    }



    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == strUpButton) {
            model.improveStr();
            view.repaint();
        } else if (e.getSource() == dexUpButton) {
            model.improveDex();
            view.repaint();
        } else if (e.getSource() == conUpButton) {
            model.improveCon();
            view.repaint();
        } else if (e.getSource() == intUpButton) {
            model.improveInt();
            view.repaint();
        } else if (e.getSource() == wisUpButton) {
            model.improveWis();
            view.repaint();
        } else if (e.getSource() == chaUpButton) {
            model.improveCha();
            view.repaint();


        }
    }






}


