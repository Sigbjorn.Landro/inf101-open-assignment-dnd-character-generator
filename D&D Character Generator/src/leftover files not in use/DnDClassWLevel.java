package DnDCharacterGenerator.DnDPlayersHandBook;

public class DnDClassWLevel {
    private final DnDClass classDD;
    private int classLevel;

    public DnDClassWLevel(DnDClass classDnD, int classLevels) {
        this.classDD = classDnD;
        this.classLevel = classLevels;

    }

    /**
     * 
     * @return
     */
    public DnDClass getDnDClass() {
        return classDD;
    }

    /**
     * 
     * @return
     */
    public int getClassLevel() {
        return classLevel;
    }

    /**
     * 
     */
    public void levelUp() {
        this.classLevel++;
    }

    /**
     * 
     * @param levels
     */
    public void levelUpBulck(int levels) {
        this.classLevel += levels;
    }
}
