package DnDCharacterGenerator.DnDPlayersHandBook;

public enum DnDClass {
    ARTIFICER,
    BARBARIAN,
    BARD,
    CLERIC,
    DRUID,
    FIGHTER,
    MONK,
    PALADIN,
    RANGER,
    ROUGUE,
    SORCERER,
    WARLOCK,
    WIZARD
}
