package DnDCharacterGenerator.DiceAndStuff;

import java.util.Random;

public class DiceRoller {

    Random seed;

    public DiceRoller() {
        this(new Random());
    }

    public DiceRoller(long seed) {
        this(new Random(seed));
    }

    public DiceRoller(Random seed) {
        this.seed = seed;
    }

    /**
     * The method that defines the DiceRoller object,
     * it rolls x amount of dice with y sides for each die
     * and sums the resaults together.
     * 
     * This makes a lot of sense in the context of
     * most table top role playing games as it is
     * very typical to roll a lot of dice with a large
     * veraiety of sides. Usually this is would be called
     * rolling XdY.
     * For example rolling 1 dice with 6 sides (one normal dice),
     * that would be called rolling 1d6,
     * or rolling 3 dice with 8 sides would be called rolling 3d8.
     * 
     * @param x amount of dice
     * @param y sides on each dice
     * @return sum of all resaults rolled
     */
    public int rollXdY(int x, int y) {
        if (x < 1 || y < 2) {
            throw new IllegalArgumentException("Amount of dice x needs to be positive and greater than 0, and sides on each dice must be positive and greater than 1.");
        }
        int roll = 0;
        for (int i = 0; i < x; i++) {
            roll += seed.nextInt(y) + 1;
        }
        return roll;
    }

    /**
     * The same as rollXdY, but returns the resault seperated as an array.
     * 
     * @param x amount of dice
     * @param y sides on each dice
     * @return resaults rolled in an array
     */
    public int[] rollXdYseperate(int x, int y) {
        if (x < 1 || y < 2) {
            throw new IllegalArgumentException("Amount of dice x needs to be positive and greater than 0, and sides on each dice must be positive and greater than 1.");
        }
        int[] rolls = new int[x];
        for (int i = 0; i < rolls.length; i++) {
            rolls[i] = rollXdY(1, y);
        }
        return rolls;
    }
    
    
}
