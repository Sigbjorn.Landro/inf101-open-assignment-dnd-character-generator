package DnDCharacterGenerator.DiceAndStuff;

import java.util.List;

import DnDCharacterGenerator.DnDPlayersHandBook.DnDBackground;
import DnDCharacterGenerator.DnDPlayersHandBook.DnDRace;
import DnDCharacterGenerator.DnDPlayersHandBook.DnDSkill;
import DnDCharacterGenerator.view.DnDCharacterMakerViewable;
import DnDCharacterGenerator.view.DnDCharacterSheetViewable;

public class StatGenModel implements DnDCharacterMakerViewable{

    StatGenerator generator;


    int[] statArray;

    int strength;
    int dexterity;
    int constitution;
    int intelligence;
    int wisdom;
    int charisma;


    public StatGenModel() {
        this(new StatGenerator());
    }

    public StatGenModel(StatGenerator generator) {
        this.generator = generator;
        this.statArray = generator.generateZeroArray();

        this.clearStats();
    }

    public void genStatArrayRandom() {
        this.statArray = generator.generateStatArray();
    }

    public void genStatArrayStandard() {
        this.statArray = generator.generateStandardArray();
    }

    

    public void clearStats() {
        this.strength = 0;
        this.dexterity = 0;
        this.constitution = 0;
        this.intelligence = 0;
        this.wisdom = 0;
        this.charisma = 0;
    }

    public void setStr(int str) {
        this.strength = str;
    }
    
    public void setDex(int dex) {
        this.dexterity = dex;
    }

    public void setCon(int con) {
        this.constitution = con;
    }

    public void setInt(int intell) {
        this.intelligence = intell;
    }

    public void setWis(int wis) {
        this.wisdom = wis;
    }

    public void setCha(int cha) {
        this.charisma = cha;
    }


    

    @Override
    public int getStr() {
        // TODO Auto-generated method stub
        return this.strength;
    }

    @Override
    public int getDex() {
        // TODO Auto-generated method stub
        return this.dexterity;
    }

    @Override
    public int getCon() {
        // TODO Auto-generated method stub
        return this.constitution;
    }

    @Override
    public int getInt() {
        // TODO Auto-generated method stub
        return this.intelligence;
    }

    @Override
    public int getWis() {
        // TODO Auto-generated method stub
        return this.wisdom;
    }

    @Override
    public int getCha() {
        // TODO Auto-generated method stub
        return this.constitution;
    }


    @Override
    public int[] getStatArray() {
        // TODO Auto-generated method stub
        return null;
    }

}
