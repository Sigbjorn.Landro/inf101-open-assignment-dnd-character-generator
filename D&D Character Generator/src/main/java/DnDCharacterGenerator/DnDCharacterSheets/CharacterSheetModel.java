package DnDCharacterGenerator.DnDCharacterSheets;

import java.util.List;

import DnDCharacterGenerator.DnDPlayersHandBook.DnDSkill;
import DnDCharacterGenerator.view.DnDCharacterSheetViewable;

public class CharacterSheetModel implements DnDCharacterSheetViewable, DnDCharacterLevelUpInterface {

    PlayerCharacter character;

    public CharacterSheetModel(PlayerCharacter character) {
        this.character = character;
    }

    @Override
    public String getName() {
        return this.character.name;
    }

    @Override
    public String getRace() {
        return this.character.race;
    }

    @Override
    public String getBackground() {
        return this.character.background;
    }

    @Override
    public String getDnDClass() {
        return this.character.getDnDClass();
    }

    @Override
    public int getLevel() {
        return this.character.classLevel;
    }

    @Override
    public int getStr() {
        return this.character.strength;
    }

    @Override
    public int getDex() {
        return this.character.dexterity;
    }

    @Override
    public int getCon() {
        return this.character.constitution;
    }

    @Override
    public int getInt() {
        return this.character.intelligence;
    }

    @Override
    public int getWis() {
        return this.character.wisdom;
    }

    @Override
    public int getCha() {
        return this.character.charisma;
    }

    @Override
    public int getHp() {
        return this.character.totalHp;
    }

    @Override
    public int getCurrentHp() {
        return this.character.currentHp;
    }

    @Override
    public int getAc() {
        return this.character.ac;
    }

    @Override
    public int getInitiative() {
        return this.character.initiative;
    }

    @Override
    public int getSpeed() {
        return this.character.speed;
    }

    @Override
    public int getProficiencyBonus() {
        return this.character.proficienciyBonus;
    }

    @Override
    public int getAbilityScoreImprovements() {
        return this.character.abilityScoreImprovements;
    }
    
    @Override
    public List<String> getCharacterFeatures() {
        return this.character.features;
    }

    @Override
    public List<DnDSkill> getSkills() {
        return this.character.skills;
    }

    @Override
    public List<DnDSkill> getAvailableSkills() {
        return this.character.classSkills;
    }

    @Override
    public int getUnassignedSkills() {
        return this.character.unassignedSkills;
    }

    @Override
    public void levelUp() {
        this.character.levelUp();
    }

    @Override
    public void improveStr() {
        this.character.improveStr();
    }

    @Override
    public void improveDex() {
        this.character.improveDex();
    }

    @Override
    public void improveCon() {
        this.character.improveCon();
    }

    @Override
    public void improveInt() {
        this.character.improveInt();
    }

    @Override
    public void improveWis() {
        this.character.improveWis();
    }

    @Override
    public void improveCha() {
        this.character.improveCha();
    }

    @Override
    public void addSkill(DnDSkill skill) {
        this.character.addSkill(skill);
    }

    @Override
    public int getStatMod(int stat) {
        return this.character.getStatMod(stat);
    }

    @Override
    public int[] getSpellSlots() {
        // TODO Auto-generated method stub
        return this.character.getSpellSlots();
    }

    

    

}
