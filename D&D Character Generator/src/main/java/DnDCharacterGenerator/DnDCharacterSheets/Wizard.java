package DnDCharacterGenerator.DnDCharacterSheets;

import java.util.ArrayList;
import java.util.List;

import DnDCharacterGenerator.DnDPlayersHandBook.DnDBackground;
import DnDCharacterGenerator.DnDPlayersHandBook.DnDRace;

public class Wizard extends PlayerCharacter{

    private int spellsLv1PerDay;
    private int spellsLv2PerDay;
    private int spellsLv3PerDay;
    private int spellsLv4PerDay;
    private int spellsLv5PerDay;
    private int spellsLv6PerDay;
    private int spellsLv7PerDay;
    private int spellsLv8PerDay;
    private int spellsLv9PerDay;

    private List spellBook;

    public Wizard(String name, DnDRace race, DnDBackground background, int str, int dex, int con, int intell, int wis,
            int cha) {
        super(name, race, background, str, dex, con, intell, wis, cha, 6);
        this.classLevel = 1;

        this.spellsLv1PerDay = 2;
        this.spellsLv2PerDay = 0;
        this.spellsLv3PerDay = 0;
        this.spellsLv4PerDay = 0;
        this.spellsLv5PerDay = 0;
        this.spellsLv6PerDay = 0;
        this.spellsLv7PerDay = 0;
        this.spellsLv8PerDay = 0;
        this.spellsLv9PerDay = 0;

        this.spellBook = new ArrayList<>();

        this.features.add("Arcane Recovery");
    }

    @Override
    public void levelUp() {
        this.classLevel++;
        this.totalHp += diceBag.rollXdY(1, 10);
        switch (this.classLevel) {
            case 2:
                this.spellsLv1PerDay++;
                break;
            case 3:
                this.spellsLv1PerDay++;
                this.spellsLv2PerDay += 2;
                break;
            case 4:
                this.spellsLv2PerDay++;
                this.abilityScoreImprovements += 2;
                break;
            case 5:
                this.proficienciyBonus++;
                this.spellsLv3PerDay += 2;
                break;
            case 6:
                this.spellsLv3PerDay++;
                this.abilityScoreImprovements += 2;
                break;
            case 7:
                this.spellsLv4PerDay++;
                break;
            case 8:
                this.spellsLv4PerDay++;
                this.abilityScoreImprovements += 2;
                break;
            case 9:
                this.proficienciyBonus++;
                this.spellsLv4PerDay++;
                this.spellsLv5PerDay++;
                break;
            case 10:
                this.spellsLv5PerDay++;
                break;
            case 11:
                this.spellsLv6PerDay++;
                break;
            case 12:
                this.abilityScoreImprovements += 2;
                break;
            case 13:
                this.proficienciyBonus++;
                this.spellsLv7PerDay++;
                break;
            case 14:
                this.abilityScoreImprovements += 2;
                break;
            case 15:
                this.spellsLv8PerDay++;
                break;
            case 16:
                this.abilityScoreImprovements += 2;
                break;
            case 17:
                this.proficienciyBonus++;
                this.spellsLv9PerDay++;
                break;
            case 18:
                this.features.add("Spell Mastery");
                break;
            case 19:
                this.abilityScoreImprovements += 2;
                break;
            case 20:
                this.features.add("Signature Spell");
                break;
            default:
                
        }
    }

    @Override
    public int[] getSpellSlots() {
        int[] spellSlots = {this.spellsLv1PerDay, this.spellsLv2PerDay, this.spellsLv3PerDay, this.spellsLv4PerDay, this.spellsLv5PerDay,
                            this.spellsLv6PerDay, this.spellsLv7PerDay, this.spellsLv8PerDay, this.spellsLv9PerDay};
        return spellSlots;
    }
    
}
