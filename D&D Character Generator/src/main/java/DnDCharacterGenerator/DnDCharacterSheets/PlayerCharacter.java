package DnDCharacterGenerator.DnDCharacterSheets;

import java.util.ArrayList;
import java.util.List;

import DnDCharacterGenerator.DiceAndStuff.DiceRoller;
import DnDCharacterGenerator.DnDPlayersHandBook.DnDBackground;
import DnDCharacterGenerator.DnDPlayersHandBook.DnDRace;
import DnDCharacterGenerator.DnDPlayersHandBook.DnDSkill;

public abstract class PlayerCharacter  implements DnDCharacterLevelUpInterface {

    String name;

    String race;
    String background;

    int totalHp;
    int currentHp;

    int strength;
    int dexterity;
    int constitution;
    int intelligence;
    int wisdom;
    int charisma;

    int ac;
    int initiative;
    int speed;

    List<DnDSkill> skills;
    List<String> features;

    DiceRoller diceBag;

    int classLevel;
    int proficienciyBonus;

    int abilityScoreImprovements;

    List<DnDSkill> classSkills;
    int unassignedSkills;
    int freeSkills;

    public PlayerCharacter(String name, DnDRace race, DnDBackground background, int str, int dex, int con, int intell, int wis, int cha, int hitDice) {
        this.name = name;

        this.strength = str;
        this.dexterity = dex;
        this.constitution = con;
        this.intelligence = intell;
        this.wisdom = wis;
        this.charisma = cha;

        this.speed = 30;

        this.skills = new ArrayList<DnDSkill>();
        this.features = new ArrayList<String>();

        this.addBackgroundSkills(background);
        this.addRaceFeatures(race);

        this.totalHp = hitDice + getStatMod(this.constitution);
        this.currentHp = this.totalHp;

        this.ac = 10 + getStatMod(this.dexterity);
        this.initiative = getStatMod(this.dexterity);

        this.diceBag = new DiceRoller();

        this.classLevel = 0;
        this.proficienciyBonus = 0;

        this.abilityScoreImprovements = 0;

        this.classSkills = new ArrayList<DnDSkill>();
        this.unassignedSkills = 0;
        this.freeSkills = 0;
        // freeSkills is not used in the current build, but it might be used in the future
        // if races that can choose one (or more) skill proficiencies, like Half Elf or Variant Human.

    }



    /**
     * Calculates the Ability Score Modifier based on a given Ability Score.
     * This Modifier is calculated by (Ability Score - 10) / 2 always rounded down.
     * 
     * @param abilityScore Ability Score to calculate the Modifier of
     * @return integer given as (abilityScore -10) / 2 rounded down
     */
    public int getStatMod(int abilityScore) {
        return Math.floorDiv(abilityScore - 10, 2);
    }

    /**
     * Adds the bonuses and features from the character's race.
     * This is only used during the creation of the character in the constructor,
     * and thus it is set as private.
     */
    private void addRaceFeatures(DnDRace race) {
        switch (race) {
            case HUMAN: // if human: +1 to all stats
                this.race = "Human";
                this.strength++;
                this.dexterity++;
                this.constitution++;
                this.intelligence++;
                this.wisdom++;
                this.charisma++;
                this.skills.add(DnDSkill.COMMON);
                break;
            case WOOD_ELF: // if wood elf: +2 dex, +1 wis and +5 movement speed
                this.race = "Wood Elf";
                this.dexterity += 2;
                this.wisdom++;
                this.speed += 5;
                if (!this.skills.contains(DnDSkill.PERCEPTION)) {
                    this.skills.add(DnDSkill.PERCEPTION);
                }
                this.features.add("Darkvision");
                this.features.add("Fey Ancestry");
                this.features.add("Trance");
                this.features.add("Mask of the Wild");
                this.skills.add(DnDSkill.COMMON);
                this.skills.add(DnDSkill.ELVEN);
                break;
            case MOUNTAIN_DWARF: // if mountain dwarf: +2 to str and con
                this.race = "Mountain Dwarf";
                this.strength += 2;
                this.constitution += 2;
                this.speed -= 5;
                this.features.add("Darkvision");
                this.skills.add(DnDSkill.COMMON);
                this.skills.add(DnDSkill.DWARVEN);
                break;
            default: // if none of the above
                this.race = "-";
        }
    }

    /**
     * Adds the skills from the character's background.
     * This is only used during the creation of the character in the constructor,
     * and thus it is set as private.
     */
    private void addBackgroundSkills(DnDBackground background) {
        switch (background) {
            case CRIMINAL:
                this.background = "Criminal";
                this.skills.add(DnDSkill.DECEPTION);
                this.skills.add(DnDSkill.STEALTH);
                this.skills.add(DnDSkill.THIEVS_TOOLS);
                this.skills.add(DnDSkill.GAMING_SET);
                break;
            case NOBLE:
                this.background = "Noble";
                this.skills.add(DnDSkill.HISTORY);
                this.skills.add(DnDSkill.PERSUASION);
                this.skills.add(DnDSkill.GAMING_SET);
                break;
            case SAGE:
                this.background = "Sage";
                this.skills.add(DnDSkill.ARCANA);
                this.skills.add(DnDSkill.HISTORY);
                break;
            case SAILOR:
                this.background = "Sailor";
                this.skills.add(DnDSkill.ATHLETICS);
                this.skills.add(DnDSkill.PERCEPTION);
                this.skills.add(DnDSkill.NAVIGATORS_TOOLS);
                this.skills.add(DnDSkill.WATER_VEHICLES);
                break;
            case SOLDIER:
                this.background = "Soldier";
                this.skills.add(DnDSkill.ATHLETICS);
                this.skills.add(DnDSkill.INTIMIDATON);
                this.skills.add(DnDSkill.GAMING_SET);
                this.skills.add(DnDSkill.LAND_VEHICLES);
                break;
            default:
                this.background = "-";
        }
    }

    /**
     * 
     * 
     * @return
     */
    public String getDnDClass() {
        return "no class";
    }

    
    @Override
    public void improveStr() {
        if (this.abilityScoreImprovements >= 1 && this.strength < 20) {
            this.strength++;
            this.abilityScoreImprovements--;
        }
    }

    @Override
    public void improveDex() {
        if (this.abilityScoreImprovements >= 1 && this.dexterity < 20) {
            this.dexterity++;
            this.abilityScoreImprovements--;
            this.ac = 10 + getStatMod(this.dexterity);
            this.initiative = getStatMod(this.dexterity);
        }
    }

    @Override
    public void improveCon() {
        if (this.abilityScoreImprovements >= 1 && this.constitution < 20) {
            this.totalHp -= getStatMod(this.constitution);
            this.currentHp -= getStatMod(this.constitution);
            this.constitution++;
            this.totalHp += getStatMod(this.constitution);
            this.currentHp += getStatMod(this.constitution);
            this.abilityScoreImprovements--;
        }
    }

    @Override
    public void improveInt() {
        if (this.abilityScoreImprovements >= 1 && this.intelligence < 20) {
            this.intelligence++;
            this.abilityScoreImprovements--;
        }
    }

    @Override
    public void improveWis() {
        if (this.abilityScoreImprovements >= 1 && this.wisdom < 20) {
            this.wisdom++;
            this.abilityScoreImprovements--;
        }
    }

    @Override
    public void improveCha() {
        if (this.abilityScoreImprovements >= 1 && this.charisma < 20) {
            this.charisma++;
            this.abilityScoreImprovements--;
        }
    }
    
    @Override
    public void addSkill(DnDSkill skill) {
        if (this.unassignedSkills >= 1 && !this.skills.contains(skill) && this.classSkills.contains(skill)) {
            this.skills.add(skill);
            this.unassignedSkills--;
        }
    }

    /**
     * 
     * @return
     */
    public int[] getSpellSlots() {
        int[] spellSlots = {0,0,0,0,0,0,0,0,0};
        return spellSlots;
    }

}
