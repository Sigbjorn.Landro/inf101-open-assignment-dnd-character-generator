package DnDCharacterGenerator.DnDCharacterSheets;

import DnDCharacterGenerator.DnDPlayersHandBook.DnDBackground;
import DnDCharacterGenerator.DnDPlayersHandBook.DnDRace;
import DnDCharacterGenerator.DnDPlayersHandBook.DnDSkill;

public class Rogue extends PlayerCharacter{

    

    public Rogue(String name, DnDRace race, DnDBackground background, int str, int dex, int con, int intell, int wis,
            int cha) {
        super(name, race, background, str, dex, con, intell, wis, cha, 8);
        this.classLevel = 1;
        this.features.add("Sneak Attack");
        this.features.add("Expertise");
        this.features.add("Thievs' Cant");
        this.unassignedSkills += 4;
        this.classSkills.add(DnDSkill.ACROBATICS);
        this.classSkills.add(DnDSkill.DECEPTION);
        this.classSkills.add(DnDSkill.ATHLETICS);
        this.classSkills.add(DnDSkill.INSIGHT);
        this.classSkills.add(DnDSkill.INTIMIDATON);
        this.classSkills.add(DnDSkill.PERCEPTION);
        this.classSkills.add(DnDSkill.STEALTH);
        this.classSkills.add(DnDSkill.SURVIVAL);
    }

    @Override
    public void levelUp() {
        this.classLevel++;
        int damage = this.totalHp - this.currentHp;
        this.totalHp += diceBag.rollXdY(1, 10);
        this.currentHp = this.totalHp - damage;
        switch (this.classLevel) {
            case 2:
                this.features.add("Cunning Action");
                break;
            case 3:
                this.features.add("Rougish Archetype");
                break;
            case 4:
                this.abilityScoreImprovements += 2;
                break;
            case 5:
                this.proficienciyBonus++;
                this.features.add("Uncanny Dodge");
                break;
            case 6:
                this.features.add("Expertise");
                break;
            case 7:
                this.features.add("Evasion");
                break;
            case 8:
                this.abilityScoreImprovements += 2;
                break;
            case 9:
                this.proficienciyBonus++;
                break;
            case 10:
                this.abilityScoreImprovements += 2;
                break;
            case 11:
                this.features.add("Reliable Talent");
                break;
            case 12:
                this.abilityScoreImprovements += 2;
                break;
            case 13:
                this.proficienciyBonus++;
                break;
            case 14:
                this.features.add("Blind Sense");
                break;
            case 15:
                this.features.add("Slippery Mind");
                break;
            case 16:
                this.abilityScoreImprovements += 2;
                break;
            case 17:
                this.proficienciyBonus++;
                break;
            case 18:
                this.features.add("Elusive");
                break;
            case 19:
                this.abilityScoreImprovements += 2;
                break;
            case 20:
                this.features.add("Stroke of Luck");
                break;
            default:
        }     
    }
    
    @Override
    public String getDnDClass() {
        return "Rouge";
    }
    
}
