package DnDCharacterGenerator.DnDCharacterSheets;

import java.util.List;

import DnDCharacterGenerator.DnDPlayersHandBook.DnDBackground;
import DnDCharacterGenerator.DnDPlayersHandBook.DnDRace;
import DnDCharacterGenerator.DnDPlayersHandBook.DnDSkill;
import DnDCharacterGenerator.view.DnDCharacterSheetViewable;

public class Fighter extends PlayerCharacter {

    public Fighter(String name, DnDRace race, DnDBackground background, int str, int dex, int con, int intell, int wis,
            int cha) {
        super(name, race, background, str, dex, con, intell, wis, cha, 10);
        this.classLevel = 1;
        this.proficienciyBonus = 2;
        this.features.add("Fighting Style");
        this.features.add("Second Wind");
        this.unassignedSkills += 2;
        this.classSkills.add(DnDSkill.ACROBATICS);
        this.classSkills.add(DnDSkill.ANIMAL_HANDLING);
        this.classSkills.add(DnDSkill.ATHLETICS);
        this.classSkills.add(DnDSkill.HISTORY);
        this.classSkills.add(DnDSkill.INSIGHT);
        this.classSkills.add(DnDSkill.INTIMIDATON);
        this.classSkills.add(DnDSkill.PERCEPTION);
        this.classSkills.add(DnDSkill.SURVIVAL);
    }

    public Fighter(String name, DnDRace race, DnDBackground background, int str, int dex, int con, int intell, int wis,
             int cha, int startingLevel) {
        this(name, race, background, str, dex, con, intell, wis, cha);
        for (int i = 0; i < startingLevel; i++) {
            this.levelUp();
        }
    }
    
    
    @Override
    public void levelUp() {
        this.classLevel++;
        int damage = this.totalHp - this.currentHp;
        this.totalHp += diceBag.rollXdY(1, 10);
        this.currentHp = this.totalHp - damage;
        switch (this.classLevel) {
            case 2:
                this.features.add("Action Surge");
                break;
            case 3:
                this.features.add("Martial Archetype");
                break;
            case 4:
                this.abilityScoreImprovements += 2;
                break;
            case 5:
                this.proficienciyBonus++;
                this.features.add("Extra Attack");
                break;
            case 6:
                this.abilityScoreImprovements += 2;
                break;
            case 7:
                break;
            case 8:
                this.abilityScoreImprovements += 2;
                break;
            case 9:
                this.proficienciyBonus++;
                this.features.add("Indomitable");
                break;
            case 10:
                break;
            case 11:
                this.features.remove("Extra Attack");
                this.features.add("Extra Attack (2)");
                break;
            case 12:
                this.abilityScoreImprovements += 2;
                break;
            case 13:
                this.proficienciyBonus++;
                this.features.remove("Indomitable");
                this.features.add("Indomitable (two uses)");
                break;
            case 14:
                this.abilityScoreImprovements += 2;
                break;
            case 15:
                break;
            case 16:
                this.abilityScoreImprovements += 2;
                break;
            case 17:
                this.proficienciyBonus++;
                this.features.remove("Action Surge");
                this.features.remove("Indomitable (two uses)");
                this.features.add("Action Surge (two uses)");
                this.features.add("Indomitable (three uses)");
                break;
            case 18:
                break;
            case 19:
                this.abilityScoreImprovements += 2;
                break;
            case 20:
                this.features.remove("Extra Attack (2)");
                this.features.add("Extra Attack (3)");
                break;
            default:
                
        }
    }

    @Override
    public String getDnDClass() {
        return "Fighter";
    }

    
}
