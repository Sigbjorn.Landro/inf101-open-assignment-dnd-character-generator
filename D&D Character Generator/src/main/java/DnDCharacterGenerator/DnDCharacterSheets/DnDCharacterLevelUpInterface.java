package DnDCharacterGenerator.DnDCharacterSheets;

import DnDCharacterGenerator.DnDPlayersHandBook.DnDSkill;

public interface DnDCharacterLevelUpInterface {
    

    /**
     * Levels up a character.
     */
    void levelUp();

    /**
     * Improves strength by one.
     */
    void improveStr();

    /**
     * Improves dexterity by one.
     */
    void improveDex();

    /**
     * Improves constitution by one.
     */
    void improveCon();

    /**
     * Improves intelligence by one.
     */
    void improveInt();

    /**
     * Improves wisdom by one.
     */
    void improveWis();

    /**
     * Improves charisma by one.
     */
    void improveCha();

    /**
     * Adds skill if both it is a class skill and the character has unassigned skills.
     */
    void addSkill(DnDSkill skill);


}
