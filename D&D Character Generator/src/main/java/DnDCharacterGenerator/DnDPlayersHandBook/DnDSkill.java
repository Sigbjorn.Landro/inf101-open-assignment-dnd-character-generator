package DnDCharacterGenerator.DnDPlayersHandBook;

public enum DnDSkill {

    // Skill proficiencies
    ACROBATICS,
    ANIMAL_HANDLING,
    ARCANA,
    ATHLETICS,
    DECEPTION,
    HISTORY,
    INSIGHT,
    INTIMIDATON,
    MEDICINE,
    NATURE,
    PERCEPTION,
    PERFORMANCE,
    PERSUASION,
    RELIGION,
    SLEIGHT_OF_HAND,
    STEALTH,
    SURVIVAL,

    // Tool proficiencies
    THIEVS_TOOLS,
    GAMING_SET,
    NAVIGATORS_TOOLS,
    WATER_VEHICLES,
    LAND_VEHICLES,

    // Language proficiencies
    COMMON,
    ELVEN,
    DWARVEN
    
}
