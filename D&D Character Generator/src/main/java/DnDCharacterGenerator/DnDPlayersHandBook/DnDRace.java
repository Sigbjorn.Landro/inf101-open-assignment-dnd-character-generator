package DnDCharacterGenerator.DnDPlayersHandBook;

public enum DnDRace {
    //DRAGONBORN,
    //HILL_DWARF,
    MOUNTAIN_DWARF,
    //DROW,
    //HIGH_ELF,
    WOOD_ELF,
    //FOREST_GNOME,
    //ROCK_GNOME,
    //HALF_ELF,
    //HALF_ORC,
    //LIGHTFOOT_HALFLING,
    //STOUT_HAFLING,
    HUMAN
    //VARIANT_HUMAN,
    //TIEFLING
}
