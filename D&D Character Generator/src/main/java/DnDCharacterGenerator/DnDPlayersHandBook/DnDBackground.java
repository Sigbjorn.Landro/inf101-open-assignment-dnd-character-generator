package DnDCharacterGenerator.DnDPlayersHandBook;

/*

Background determines some of the starting proficiencies of a character as well as some starting equipment.

*/

/**
 * 
 */
public enum DnDBackground {
    //ACOLYTE,
    //CHARLATAN,
    CRIMINAL,
    //ENTERTAINER,
    //FOLK_HERO,
    //GUILD_ARTISAN,
    //HERMIT,
    NOBLE,
    //OUTLANDER,
    SAGE,
    SAILOR,
    SOLDIER
    //URCHIN
}
