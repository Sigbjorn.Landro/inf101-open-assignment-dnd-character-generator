package DnDCharacterGenerator.view;

public interface DnDCharacterMakerViewable {
    

    /**
     * Gets the strength score of the character.
     * 
     * @return
     */
    int getStr();

    /**
     * 
     * @return
     */
    int getDex();


    /**
     * 
     * @return
     */
    int getCon();


    /**
     * 
     * @return
     */
    int getInt();

    /**
     * 
     * @return
     */
    int getWis();

    /**
     * 
     * @return
     */
    int getCha();

    
    /**
     * 
     * @return
     */
    int[] getStatArray();



}
