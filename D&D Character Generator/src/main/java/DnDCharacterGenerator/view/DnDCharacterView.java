package DnDCharacterGenerator.view;

import javax.swing.JButton;
import javax.swing.JComponent;

import DnDCharacterGenerator.DnDPlayersHandBook.DnDSkill;

import java.awt.Graphics;
import java.util.List;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

public class DnDCharacterView extends JComponent {

    {
        // This code (between curly braces) is executed when an object is
        // created (before the call to the constructor, if one exists). 
        
        // The call to setFocusable enables the panel to receive events from
        // the user, such as key-presses and mouse movements.
        this.setFocusable(true);
    }

    
    DnDCharacterSheetViewable character;


    public DnDCharacterView(DnDCharacterSheetViewable model) {
        this.character = model;
    }
    

    @Override
    public void paint(Graphics canvas) {
        // This will call the paint -method of the JComponent class
        // It is often a good practice to call the super method when
        // overriding an existing method from an inherited class --
        // it might be doing something important.
        super.paint(canvas);

        // Text placed on the same exact same coordinate as the "taller rectangle"
        // Surprised at where it shows up?
        canvas.setColor(Color.BLACK);
        Font myFont = new Font("SansSerif", Font.BOLD, 16);
        Font smallerFont = new Font("SansSerif", Font.BOLD, 12);
        Font largerFont = new Font("SansSerif", Font.BOLD, 22);
        canvas.setFont(myFont);

        canvas.drawString("Name: " + this.character.getName(), 20, 20);
        canvas.drawString("Race: " + this.character.getRace(), 20, 40);
        canvas.drawString("Background: " + this.character.getBackground(), 20, 60);
        canvas.drawString("Class: " + this.character.getDnDClass() + " Lv. " + this.character.getLevel(), 20, 80);
        //canvas.

        canvas.setFont(smallerFont);
        canvas.drawString("Stat:",75,95);
        canvas.drawString("Mod:",125,95);
        canvas.setFont(largerFont);

        canvas.drawString("Str:",20,115);
        canvas.drawString("Dex:",20,135);
        canvas.drawString("Con:",20,155);
        canvas.drawString("Int:",20,175);
        canvas.drawString("Wis:",20,195);
        canvas.drawString("Cha:",20,215);

        canvas.drawString(""+ this.character.getStr(),75,115);
        canvas.drawString(""+ this.character.getDex(),75,135);
        canvas.drawString(""+ this.character.getCon(),75,155);
        canvas.drawString(""+ this.character.getInt(),75,175);
        canvas.drawString(""+ this.character.getWis(),75,195);
        canvas.drawString(""+ this.character.getCha(),75,215);
        canvas.setFont(smallerFont);
        canvas.drawString("Ability Score Improvements: " + this.character.getAbilityScoreImprovements(),20,230);

        canvas.setFont(largerFont);
        canvas.drawString(GraphicHelperMethods.intToStringWithSign(this.character.getStatMod(this.character.getStr())),125,115);
        canvas.drawString(GraphicHelperMethods.intToStringWithSign(this.character.getStatMod(this.character.getDex())),125,135);
        canvas.drawString(GraphicHelperMethods.intToStringWithSign(this.character.getStatMod(this.character.getCon())),125,155);
        canvas.drawString(GraphicHelperMethods.intToStringWithSign(this.character.getStatMod(this.character.getInt())),125,175);
        canvas.drawString(GraphicHelperMethods.intToStringWithSign(this.character.getStatMod(this.character.getWis())),125,195);
        canvas.drawString(GraphicHelperMethods.intToStringWithSign(this.character.getStatMod(this.character.getCha())),125,215);

        canvas.drawString("Hit Points: " + this.character.getCurrentHp() + " / " + this.character.getHp(),240,115);
        canvas.drawString("Armour Class: " + this.character.getAc(),240,135);
        canvas.drawString("Initiative: " + GraphicHelperMethods.intToStringWithSign(this.character.getInitiative()),240,155);
        canvas.drawString("Speed: " + this.character.getSpeed() + "ft.",240,175);
        canvas.drawString("Proificiency Bonus: " + GraphicHelperMethods.intToStringWithSign(this.character.getProficiencyBonus()),240,195);

        canvas.setFont(smallerFont);
        canvas.drawString("Skills:",75,255);
        canvas.drawString("Unassigned Skills : " + character.getUnassignedSkills(),20,605);
        canvas.setFont(myFont);
        canvas.drawString("Acrobatics (Dex):",40,270);
        canvas.drawString("Animal Handling (Wis):",40,290);
        canvas.drawString("Arcana (Int):",40,310);
        canvas.drawString("Athletics (Str):",40,330);
        canvas.drawString("Deception (Cha):",40,350);
        canvas.drawString("History (Int):",40,370);
        canvas.drawString("Insight (Wis):",40,390);
        canvas.drawString("Intimidaton (Cha):",40,410);
        canvas.drawString("Medicine (Wis):",40,430);
        canvas.drawString("Nature (Int):",40,450);
        canvas.drawString("Perception (Wis):",40,470);
        canvas.drawString("Performance (Cha):",40,490);
        canvas.drawString("Persuasion (Cha):",40,510);
        canvas.drawString("Religion (Int):",40,530);
        canvas.drawString("Sleight of Hand (Dex):",40,550);
        canvas.drawString("Stealth (Dex):",40,570);
        canvas.drawString("Survival (Wis):",40,590);


        if (this.character.getSkills().contains(DnDSkill.ACROBATICS)) {
            canvas.drawString("X",20,270);
            canvas.drawString(GraphicHelperMethods.intToStringWithSign(this.character.getStatMod(this.character.getDex()) + this.character.getProficiencyBonus()),220,270);
        } else {
            canvas.drawString(GraphicHelperMethods.intToStringWithSign(this.character.getStatMod(this.character.getDex())),220,270);
        }
        if (this.character.getSkills().contains(DnDSkill.ANIMAL_HANDLING)) {
            canvas.drawString("X",20,290);
            canvas.drawString(GraphicHelperMethods.intToStringWithSign(this.character.getStatMod(this.character.getWis()) + this.character.getProficiencyBonus()),220,290);
        } else {
            canvas.drawString(GraphicHelperMethods.intToStringWithSign(this.character.getStatMod(this.character.getWis())),220,290);
        }
        if (this.character.getSkills().contains(DnDSkill.ARCANA)) {
            canvas.drawString("X",20,310);
            canvas.drawString(GraphicHelperMethods.intToStringWithSign(this.character.getStatMod(this.character.getInt()) + this.character.getProficiencyBonus()),220,310);
        } else {
            canvas.drawString(GraphicHelperMethods.intToStringWithSign(this.character.getStatMod(this.character.getInt())),220,310);
        }
        if (this.character.getSkills().contains(DnDSkill.ATHLETICS)) {
            canvas.drawString("X",20,330);
            canvas.drawString(GraphicHelperMethods.intToStringWithSign(this.character.getStatMod(this.character.getStr()) + this.character.getProficiencyBonus()),220,330);
        } else {
            canvas.drawString(GraphicHelperMethods.intToStringWithSign(this.character.getStatMod(this.character.getStr())),220,330);
        }
        if (this.character.getSkills().contains(DnDSkill.DECEPTION)) {
            canvas.drawString("X",20,350);
            canvas.drawString(GraphicHelperMethods.intToStringWithSign(this.character.getStatMod(this.character.getCha()) + this.character.getProficiencyBonus()),220,350);
        } else {
            canvas.drawString(GraphicHelperMethods.intToStringWithSign(this.character.getStatMod(this.character.getCha())),220,350);
        }
        if (this.character.getSkills().contains(DnDSkill.HISTORY)) {
            canvas.drawString("X",20,370);
            canvas.drawString(GraphicHelperMethods.intToStringWithSign(this.character.getStatMod(this.character.getInt()) + this.character.getProficiencyBonus()),220,370);
        } else {
            canvas.drawString(GraphicHelperMethods.intToStringWithSign(this.character.getStatMod(this.character.getInt())),220,370);
        }
        if (this.character.getSkills().contains(DnDSkill.INSIGHT)) {
            canvas.drawString("X",20,390);
            canvas.drawString(GraphicHelperMethods.intToStringWithSign(this.character.getStatMod(this.character.getWis()) + this.character.getProficiencyBonus()),220,390);
        } else {
            canvas.drawString(GraphicHelperMethods.intToStringWithSign(this.character.getStatMod(this.character.getWis())),220,390);
        }
        if (this.character.getSkills().contains(DnDSkill.INTIMIDATON)) {
            canvas.drawString("X",20,410);
            canvas.drawString(GraphicHelperMethods.intToStringWithSign(this.character.getStatMod(this.character.getCha()) + this.character.getProficiencyBonus()),220,410);
        } else {
            canvas.drawString(GraphicHelperMethods.intToStringWithSign(this.character.getStatMod(this.character.getCha())),220,410);
        }
        if (this.character.getSkills().contains(DnDSkill.MEDICINE)) {
            canvas.drawString("X",20,430);
            canvas.drawString(GraphicHelperMethods.intToStringWithSign(this.character.getStatMod(this.character.getWis()) + this.character.getProficiencyBonus()),220,430);
        } else {
            canvas.drawString(GraphicHelperMethods.intToStringWithSign(this.character.getStatMod(this.character.getWis())),220,430);
        }
        if (this.character.getSkills().contains(DnDSkill.NATURE)) {
            canvas.drawString("X",20,450);
            canvas.drawString(GraphicHelperMethods.intToStringWithSign(this.character.getStatMod(this.character.getInt()) + this.character.getProficiencyBonus()),220,450);
        } else {
            canvas.drawString(GraphicHelperMethods.intToStringWithSign(this.character.getStatMod(this.character.getInt())),220,450);
        }
        if (this.character.getSkills().contains(DnDSkill.PERCEPTION)) {
            canvas.drawString("X",20,470);
            canvas.drawString(GraphicHelperMethods.intToStringWithSign(this.character.getStatMod(this.character.getWis()) + this.character.getProficiencyBonus()),220,470);
        } else {
            canvas.drawString(GraphicHelperMethods.intToStringWithSign(this.character.getStatMod(this.character.getWis())),220,470);
        }
        if (this.character.getSkills().contains(DnDSkill.PERFORMANCE)) {
            canvas.drawString("X",20,490);
            canvas.drawString(GraphicHelperMethods.intToStringWithSign(this.character.getStatMod(this.character.getCha()) + this.character.getProficiencyBonus()),220,490);
        } else {
            canvas.drawString(GraphicHelperMethods.intToStringWithSign(this.character.getStatMod(this.character.getCha())),220,490);
        }
        if (this.character.getSkills().contains(DnDSkill.PERSUASION)) {
            canvas.drawString("X",20,510);
            canvas.drawString(GraphicHelperMethods.intToStringWithSign(this.character.getStatMod(this.character.getCha()) + this.character.getProficiencyBonus()),220,510);
        } else {
            canvas.drawString(GraphicHelperMethods.intToStringWithSign(this.character.getStatMod(this.character.getCha())),220,510);
        }
        if (this.character.getSkills().contains(DnDSkill.RELIGION)) {
            canvas.drawString("X",20,530);
            canvas.drawString(GraphicHelperMethods.intToStringWithSign(this.character.getStatMod(this.character.getInt()) + this.character.getProficiencyBonus()),220,530);
        } else {
            canvas.drawString(GraphicHelperMethods.intToStringWithSign(this.character.getStatMod(this.character.getInt())),220,530);
        }
        if (this.character.getSkills().contains(DnDSkill.SLEIGHT_OF_HAND)) {
            canvas.drawString("X",20,550);
            canvas.drawString(GraphicHelperMethods.intToStringWithSign(this.character.getStatMod(this.character.getDex()) + this.character.getProficiencyBonus()),220,550);
        } else {
            canvas.drawString(GraphicHelperMethods.intToStringWithSign(this.character.getStatMod(this.character.getDex())),220,550);
        }
        if (this.character.getSkills().contains(DnDSkill.STEALTH)) {
            canvas.drawString("X",20,570);
            canvas.drawString(GraphicHelperMethods.intToStringWithSign(this.character.getStatMod(this.character.getDex()) + this.character.getProficiencyBonus()),220,570);
        } else {
            canvas.drawString(GraphicHelperMethods.intToStringWithSign(this.character.getStatMod(this.character.getDex())),220,570);
        }
        if (this.character.getSkills().contains(DnDSkill.SURVIVAL)) {
            canvas.drawString("X",20,590);
            canvas.drawString(GraphicHelperMethods.intToStringWithSign(this.character.getStatMod(this.character.getWis()) + this.character.getProficiencyBonus()),220,590);
        } else {
            canvas.drawString(GraphicHelperMethods.intToStringWithSign(this.character.getStatMod(this.character.getWis())),220,590);
        }


        canvas.setFont(smallerFont);
        canvas.drawString("Features:",330,255);
        canvas.setFont(myFont);
        drawListOfStringsDownwardsAlligned(this.character.getCharacterFeatures(), 330, 270, 20, canvas);

    }

    /**
     * 
     * 
     * @param strList
     * @param x
     * @param y
     * @param spacing
     * @param canvas
     */
    private void drawListOfStringsDownwardsAlligned(List<String> strList, int x, int y, int spacing, Graphics canvas) {
        int currentY = y;
        for (String str : strList) {
            canvas.drawString(str,x,currentY);
            currentY += spacing;
        }
    }
    

    @Override
    public Dimension getPreferredSize() {
        // This method returns the "preferred" size of the component. However, if 
        // the user resize the window, the values returned here will not matter.
        // Hence, do not use the return value from here to calculate the size of
        // your components; use this.getWidht() and this.getHeight() instead.
        int width = 750;
        int height = 750;
        return new Dimension(width, height);
    }

}
