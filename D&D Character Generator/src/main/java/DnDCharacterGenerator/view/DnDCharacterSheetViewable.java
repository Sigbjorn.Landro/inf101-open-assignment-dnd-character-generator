package DnDCharacterGenerator.view;

import java.util.List;

import DnDCharacterGenerator.DnDPlayersHandBook.DnDBackground;
import DnDCharacterGenerator.DnDPlayersHandBook.DnDRace;
import DnDCharacterGenerator.DnDPlayersHandBook.DnDSkill;

public interface DnDCharacterSheetViewable {

    /**
     * Gets the name of the character as a string.
     * 
     * @return string of characters
     */
    String getName();

    /**
     * Gets the race of the character.
     * 
     * @return DnDRace enum value
     */
    String getRace();

    /**
     * Gets the background of the character.
     * 
     * @return DnDBackground enum value
     */
    String getBackground();

    /**
     * Gets the class of the character as a string.
     * 
     * @return string of characters
     */
    String getDnDClass();

    /**
     * Gets the total level of the character.
     * 
     * @return character level as integer
     */
    int getLevel();

    /**
     * Gets the strength score of the character.
     * 
     * @return
     */
    int getStr();

    

    /**
     * 
     * @return
     */
    int getDex();


    /**
     * 
     * @return
     */
    int getCon();


    /**
     * 
     * @return
     */
    int getInt();

    /**
     * 
     * @return
     */
    int getWis();

    /**
     * 
     * @return
     */
    int getCha();

    /**
     * 
     * @param stat
     * @return
     */
    int getStatMod(int stat);
    
    /**
     * 
     * @return
     */
    int getHp();

    /**
     * 
     * @return
     */
    int getCurrentHp();

    /**
     * 
     * @return
     */
    int getAc();

    /**
     * 
     * @return
     */
    int getInitiative();

    /**
     * 
     * @return
     */
    int getSpeed();

    /**
     * 
     * @return
     */
    int getProficiencyBonus();

    /**
     * 
     * 
     * @return
     */
    int getAbilityScoreImprovements();

    /**
     * 
     * 
     * @return
     */
    List<String> getCharacterFeatures();

    /**
     * 
     * 
     * @return
     */
    List<DnDSkill> getSkills();

    /**
     * 
     * 
     * @return
     */
    List<DnDSkill> getAvailableSkills();

    /**
     * 
     * 
     * @return
     */
    int getUnassignedSkills();


    /**
     * 
     */
    int[] getSpellSlots();
}
