package DnDCharacterGenerator.view;

import javax.swing.JButton;
import javax.swing.JComponent;

import DnDCharacterGenerator.DiceAndStuff.StatGenModel;
import DnDCharacterGenerator.DiceAndStuff.StatGenerator;
import DnDCharacterGenerator.DnDPlayersHandBook.DnDSkill;

import java.awt.Graphics;
import java.util.List;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

public class DnDCharacterMakerView extends JComponent{
    
    {
        // This code (between curly braces) is executed when an object is
        // created (before the call to the constructor, if one exists). 
        
        // The call to setFocusable enables the panel to receive events from
        // the user, such as key-presses and mouse movements.
        this.setFocusable(true);
    }

    public DnDCharacterMakerViewable model;

    

    public DnDCharacterMakerView(DnDCharacterMakerViewable model) {
        this.model = model;
    }

    
    @Override
    public void paint(Graphics canvas) {
        // This will call the paint -method of the JComponent class
        // It is often a good practice to call the super method when
        // overriding an existing method from an inherited class --
        // it might be doing something important.
        super.paint(canvas);

        // Text placed on the same exact same coordinate as the "taller rectangle"
        // Surprised at where it shows up?
        canvas.setColor(Color.BLACK);
        Font myFont = new Font("SansSerif", Font.BOLD, 16);
        Font smallerFont = new Font("SansSerif", Font.BOLD, 12);
        Font largerFont = new Font("SansSerif", Font.BOLD, 22);
        canvas.setFont(myFont);

        canvas.drawString("Name: ", 20, 20);
        canvas.drawString("Race: ", 20, 40);
        canvas.drawString("Background: ", 20, 60);
        canvas.drawString("Class: ", 20, 80);
        //canvas.

        canvas.setFont(smallerFont);
        canvas.drawString("Stat:",75,95);
        canvas.setFont(largerFont);

        canvas.drawString("Str:",20,115);
        canvas.drawString("Dex:",20,135);
        canvas.drawString("Con:",20,155);
        canvas.drawString("Int:",20,175);
        canvas.drawString("Wis:",20,195);
        canvas.drawString("Cha:",20,215);

        canvas.drawString(""+ this.model.getStr(),75,115);
        canvas.drawString(""+ this.model.getDex(),75,135);
        canvas.drawString(""+ this.model.getCon(),75,155);
        canvas.drawString(""+ this.model.getInt(),75,175);
        canvas.drawString(""+ this.model.getWis(),75,195);
        canvas.drawString(""+ this.model.getCha(),75,215);

        /*
        canvas.drawString("" + this.model.getStatArray()[0] + " - " + this.model.getStatArray()[1] + 
                            " - " + this.model.getStatArray()[2] + " - " + this.model.getStatArray()[3] + 
                            " - " + this.model.getStatArray()[4] + " - " + this.model.getStatArray()[5],20,235);
        */

        /*
        int[] aaa = new StatGenerator().generateStatArray();
        int[] bbb = new StatGenerator().generateStandardArray();
        int[] ccc = new StatGenerator().generateZeroArray();
        canvas.drawString("" + this.model.getStatArray() + ", " + aaa[0] + " - " + aaa[1] + " - " + aaa[2]  + " - " + aaa[3]  + " - " + aaa[4]  + " - " + aaa[5]  + ", " + bbb[0] + " - " + bbb[1] + " - " + bbb[2] + " - " + bbb[3] + " - " + bbb[4] + " - " + bbb[5] + ", " + ccc[0] + " - " + ccc[1] + " - " + ccc[2]  + " - " + ccc[3]  + " - " + ccc[4]  + " - " + ccc[5] ,20,235);
        */


    }

    /**
     * 
     * 
     * @param strList
     * @param x
     * @param y
     * @param spacing
     * @param canvas
     */
    private void drawListOfStringsDownwardsAlligned(List<String> strList, int x, int y, int spacing, Graphics canvas) {
        int currentY = y;
        for (String str : strList) {
            canvas.drawString(str,x,currentY);
            currentY += spacing;
        }
    }
    

    @Override
    public Dimension getPreferredSize() {
        // This method returns the "preferred" size of the component. However, if 
        // the user resize the window, the values returned here will not matter.
        // Hence, do not use the return value from here to calculate the size of
        // your components; use this.getWidht() and this.getHeight() instead.
        int width = 750;
        int height = 750;
        return new Dimension(width, height);
    }

}
