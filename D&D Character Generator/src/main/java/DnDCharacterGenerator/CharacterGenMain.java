package DnDCharacterGenerator;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JTextArea;

import DnDCharacterGenerator.DiceAndStuff.StatGenModel;
import DnDCharacterGenerator.DnDCharacterSheets.CharacterSheetModel;
import DnDCharacterGenerator.DnDCharacterSheets.Fighter;
import DnDCharacterGenerator.DnDCharacterSheets.PlayerCharacter;
import DnDCharacterGenerator.DnDPlayersHandBook.DnDBackground;
import DnDCharacterGenerator.DnDPlayersHandBook.DnDRace;
import DnDCharacterGenerator.DnDPlayersHandBook.DnDSkill;
import DnDCharacterGenerator.view.DnDCharacterMakerView;
import DnDCharacterGenerator.view.DnDCharacterMakerViewable;
import DnDCharacterGenerator.view.DnDCharacterSheetViewable;
import DnDCharacterGenerator.view.DnDCharacterView;
import DnDCharacterGenerator.view.SampleView;
import java.awt.event.ActionEvent;

import java.awt.Color;
import java.awt.event.ActionListener;

public class CharacterGenMain {
    public static final String WINDOW_TITLE = "INF101 Open Assignment - DnD Character Generator";

    public static void main(String[] args) {

        PlayerCharacter testCharacter = new Fighter("ser Bob of Semple", DnDRace.WOOD_ELF, DnDBackground.SOLDIER,
                                                        15, 13, 14, 8, 12, 10);

        CharacterSheetModel model = new CharacterSheetModel(testCharacter);


        DnDCharacterView view = new DnDCharacterView((DnDCharacterSheetViewable) model);

        StatGenModel generatingModel = new StatGenModel();
        DnDCharacterMakerView makerView = new DnDCharacterMakerView((DnDCharacterMakerViewable)generatingModel);
        
        // The JFrame is the "root" application window.
        // We here set som properties of the main window, 
        // and tell it to display our tetrisView
        JFrame frame = new JFrame(WINDOW_TITLE);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        
        /*

        frame.setContentPane(makerView);

        JTextArea nameSelectArea = new JTextArea();
        nameSelectArea.setBounds(75,5,175,20);
        frame.add(nameSelectArea);
        String[] dndRaces = {"Human", "Wood Elf", "Mountain Dwarf"};
        DnDRace[] dndRacesTrue = {DnDRace.HUMAN, DnDRace.WOOD_ELF, DnDRace.MOUNTAIN_DWARF};
        JComboBox dndRaceSelect = new JComboBox(dndRaces);
        dndRaceSelect.setBounds(125, 25,125,20);
        frame.add(dndRaceSelect);
        String[] dndBackgrounds = {"Criminal", "Noble", "Sage", "Sailor", "Soldier"};
        DnDBackground[] dnDBackgroundsTrue = {DnDBackground.CRIMINAL, DnDBackground.NOBLE, DnDBackground.SAGE, DnDBackground.SAILOR, DnDBackground.SOLDIER};
        JComboBox dndBackgroundSelect = new JComboBox(dndBackgrounds);
        dndBackgroundSelect.setBounds(125, 45,125,20);
        frame.add(dndBackgroundSelect);
        String[] dndClasses = {"Fighter", "Wizard", "Rouge"};
        JComboBox dndClassSelect = new JComboBox(dndClasses);
        dndClassSelect.setBounds(125, 65,125,20);
        frame.add(dndClassSelect);

        JButton applyButton = new JButton("Apply");
        applyButton.setBounds(370, 50, 90, 35);
        frame.add(applyButton);
        applyButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // TODO Auto-generated method stub
                /*
                generatingModel.assignName(nameSelectArea.getText());
                generatingModel.assignRace(dndRacesTrue[dndRaceSelect.getSelectedIndex()]);
                generatingModel.assignBackground(dnDBackgroundsTrue[dndBackgroundSelect.getSelectedIndex()]);
                generatingModel.assignDnDClassString(dndClasses[dndClassSelect.getSelectedIndex()]);
                *//*
                view.repaint();
            }
        });

        // Choosing between random and standard array buttons
        JButton randomArrayButton = new JButton("Random Array");
        randomArrayButton.setBounds(20, 255, 120, 35);
        frame.add(randomArrayButton);
        randomArrayButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // TODO Auto-generated method stub
                generatingModel.clearStats();
                generatingModel.genStatArrayRandom();
                
                view.repaint();
            }
        });
        JButton standardArrayButton = new JButton("Standard Array");
        standardArrayButton.setBounds(140, 255, 120, 35);
        frame.add(standardArrayButton);
        standardArrayButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // TODO Auto-generated method stub
                generatingModel.clearStats();
                generatingModel.genStatArrayStandard();
                
                view.repaint();
            }
        });
        

        frame.pack();
        frame.setVisible(true);
        
        */



        
        
        

        // Here we set which component to view in our window
        frame.setContentPane(view);

        // This JButton levels up the character
        JButton levelUpButton = new JButton("Level Up");
        levelUpButton.setBounds(240, 50, 90, 40);
        frame.add(levelUpButton);
        levelUpButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // TODO Auto-generated method stub
                model.levelUp();
                view.repaint();
            }
        });

        // JButtons adding the ASI to the stat you want
        JButton strUpButton = new JButton("Str +");
        strUpButton.setBounds(160,95, 70, 20);
        frame.add(strUpButton);
        strUpButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // TODO Auto-generated method stub
                model.improveStr();
                view.repaint();
            }
        });
        JButton dexUpButton = new JButton("Dex +");
        dexUpButton.setBounds(160,115, 70, 20);
        frame.add(dexUpButton);
        dexUpButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // TODO Auto-generated method stub
                model.improveDex();
                view.repaint();
            }
        });
        JButton conUpButton = new JButton("Con +");
        conUpButton.setBounds(160,135, 70, 20);
        frame.add(conUpButton);
        conUpButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // TODO Auto-generated method stub
                model.improveCon();
                view.repaint();
            }
        });
        JButton intUpButton = new JButton("Int +");
        intUpButton.setBounds(160,155, 70, 20);
        frame.add(intUpButton);
        intUpButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // TODO Auto-generated method stub
                model.improveInt();
                view.repaint();
            }
        });
        JButton wisUpButton = new JButton("Wis +");
        wisUpButton.setBounds(160,175, 70, 20);
        frame.add(wisUpButton);
        wisUpButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // TODO Auto-generated method stub
                model.improveWis();
                view.repaint();
            }
        });
        JButton chaUpButton = new JButton("Cha +");
        chaUpButton.setBounds(160,195, 70, 20);
        frame.add(chaUpButton);
        chaUpButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // TODO Auto-generated method stub
                model.improveCha();
                view.repaint();
            }
        });
        
        // JComboBox and JButton for assigning skills
        String[] dndSkills = {"Acrobatics", "Animal Handling", "Arcana", "Athletics", "Deception", "History", "Insight", "Intimidation",
                                "Medicine", "Nature", "Perception", "Performance", "Persuasion", "Religion", "Sleight of Hand",
                                "Stealth", "Survival"};
        DnDSkill[] dndSkillsTrue = {DnDSkill.ACROBATICS, DnDSkill.ANIMAL_HANDLING, DnDSkill.ARCANA, DnDSkill.ATHLETICS, DnDSkill.DECEPTION,
                                     DnDSkill.HISTORY, DnDSkill.INSIGHT, DnDSkill.INTIMIDATON, DnDSkill.MEDICINE, DnDSkill.NATURE, 
                                     DnDSkill.PERCEPTION, DnDSkill.PERFORMANCE, DnDSkill.PERSUASION, DnDSkill.RELIGION, DnDSkill.SLEIGHT_OF_HAND,
                                     DnDSkill.STEALTH, DnDSkill.SURVIVAL};
        JComboBox dndSkillSelect = new JComboBox(dndSkills);
        dndSkillSelect.setBounds(40, 615,170,20);
        frame.add(dndSkillSelect);
        JButton assSkillButton = new JButton("Assign");
        assSkillButton.setBounds(250,615, 75, 20);
        frame.add(assSkillButton);
        assSkillButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // TODO Auto-generated method stub
                model.addSkill(dndSkillsTrue[dndSkillSelect.getSelectedIndex()]);
                view.repaint();
            }
        });
        
        

        // Call these methods to actually display the window
        frame.pack();
        frame.setVisible(true);
        
        

        

    }
}
