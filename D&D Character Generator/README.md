D&D Character Generator
===


This program is meant as a tool for creating a character for the tabletop roleplaying game
Dungeons and Dragons 5th Edition. Due to time time constraints and scaling it down to a
reasonable work load the classes are limited to 
* Fighter
* Wizard and
* Rogue

and races are limited to
* Human
* Wood Elf and
* Mountain Dwarf

and backgrounds are limited to
* Criminal
* Noble
* Sage
* Sailor and
* Soldier



